import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {}

  // variables
  // -----------------------------------------------------------------------------

  // word Object
  idNumber    : number;
  letter      : string;
  hint        : string;
  definition  : string;
  word        : string;
  correct     : boolean;

  // game global
  count           : number = 0; // Counter for answered words
  remainingWords  : number = 25;
  title           : string = '';
  score           : number = 0;
  endSubs         : string = '';
  userAnswer      : string  = '';

  // New game
  ngControlsHideClass     : any = [];
  ngQuestionControlsHide  : any = ['hidden'];
  ngCloseHideClass        : any = ['hidden'];
  ngPaControlsHideClass   : any = ['hidden'];

  //  Timeout
  seconds : number = 150;
  intvlCountdowm : any;
   
  words: any[]  = [
    new this.Word(0, "A", "Empieza por A:", " Relato breve de un acontecimiento extraño, curioso o divertido, generalmente ocurrido a la persona que lo cuenta.", "Anecdota"),
    new this.Word(1, "B", "Empieza por B:", " Pasta dulce y esponjosa, hecha con harina, huevos, levadura y otros ingredientes, que puede tener distintas formas", "Bollo"),
    new this.Word(2, "C", "Empieza por C:", " Corriente de agua que cae desde cierta altura a causa de un brusco desnivel en su cauce, especialmente en un rio", "Cascada"),
    new this.Word(3, "D", "Empieza por D:", " Arma blanca de hoja corta, ancha y puntiaguda, parecida a la espada pero de menor tamaño", "Daga"),
    new this.Word(4, "E", "Empieza por E:", " Línea curva que describe varias vueltas alrededor de un punto, alejándose cada vez más de él", "Espiral"),
    new this.Word(5, "F", "Contiene la F:", " Que está descompuesto o podrido por la acción de diversos factores y determinados microorganismos", "Putrefacto"),
    new this.Word(6, "G", "Empieza por G:", " Que se comporta de manera ruda, tosca o grosera", "Garrulo"),
    new this.Word(7, "H", "Contiene la H:", " Persona o animal que es grueso y de poca altura", "Rechoncho"),
    new this.Word(8, "I", "Empieza por I:", " Que está en el espacio existente entre dos astros, o que tiene relación con él", "Interestelar"),
    new this.Word(9, "J", "Empieza por J:", " Chile picante de unos 5 cm de largo, carnoso y de punta redonda, que se usa para condimentar ciertos guisos", "Jalapeño"),
    new this.Word(10, "L", "Contiene la L:", " Hombre pequeño y débil", "Homunculo"),
    new this.Word(11, "M", "Empieza por M:", " Persona que sufre o muere por defender su religión o sus ideales. ", "Martir"),
    new this.Word(12, "N", "Empieza por N:", " Tubo fluorescente que produce una luz brillante.", "Neon"),
    new this.Word(13, "Ñ", "Contiene la Ñ:", " Dar a una cosa un color distinto del que tiene.", "Teñir"),
    new this.Word(14, "O", "Empieza por O:", " Que conoce todas las cosas reales y posibles.", "Omnisciente"),
    new this.Word(15, "P", "Contiene la P:", " Calzado de lona, con suela de esparto, cáñamo o goma, que se sujeta al pie por presión o con unas cintas que se atan al tobillo.", "Alpargata"),
    new this.Word(16, "Q", "Empieza por Q:", " Que se puede romper fácilmente.", "Quebradizo"),
    new this.Word(17, "R", "Empieza por R:", " Operación quirúrgica para restaurar la nariz.", "Rinoplastia"),
    new this.Word(18, "S", "Contiene la S:", " Falta de cuidado en la forma de vestir y en el aseo personal.", "Desaliño"),
    new this.Word(19, "T", "Empieza por T:", " Persona alocada, bulliciosa y molesta.", "Tabardillo"),
    new this.Word(20, "U", "Contiene la U:", " Persona que rehúye el trato de otras personas y rechaza las atenciones y muestras de cariño.", "Huraño"),
    new this.Word(21, "V", "Empieza por V:", " Tributo que el vasallo pagaba a su señor o servicio que le prestaba según este vínculo.", "Vasallaje"),
    new this.Word(22, "X", "Contiene la X:", " Punto culminante o de mayor satisfacción de la excitación sexual en las zonas erógenas o sexuales.", "Climax"),
    new this.Word(23, "Y", "Contiene la Y:", " Toro castrado, que se utiliza como animal de tiro y del cual se aprovecha su carne.", "Buey"),
    new this.Word(24, "Z", "Contiene la Z:", " Que es tonto o tiene poca rapidez mental.", "Pazguato")
  ];
  
  // functions
  // -----------------------------------------------------------------------------

   Word(idNumber, letter, hint, definition, word) : void{
    this.idNumber   = idNumber;
    this.letter     = letter;
    this.hint       = hint;
    this.definition = definition;
    this.word       = word;
    this.correct    = null;
  }
  
  showDefinition(pos) {
    this.hint = this.words[pos].hint;
    this.definition = this.words[pos].definition;
  }
  
  
  checkAnswer(pos) {
    var item = document.querySelectorAll("#lst li");
    if (this.userAnswer.toLocaleLowerCase() == this.words[pos].word.toLowerCase()) {
      this.words[pos].correct = true;
      item[pos].classList.add("item--success");
    } else {
      this.words[pos].correct = false;
      item[pos].classList.add("item--failure");
    }
    this.remainingWords--;
    this.score = this.remainingWords;
    return this.count++;
  } 

  pasapalabra(pos) {
    var w = this.words.splice(pos, 1)[0];
    this.words.push(w);
  }

   continuePlaying() {
    if (this.count != 25) {
      this.userAnswer = '';
      this.showDefinition(this.count);
    } else {
      this.endGame();
    }
  }
  
  countdown() {
    this.intvlCountdowm =  setInterval(() => { 
      this.secondsDown(); 
    }, 1000);
  }

  secondsDown(){
    this.seconds = this.seconds-1;
    if (this.seconds == 0) {
      this.endGame();
      clearInterval(this.intvlCountdowm);
      return;
    }
  }
  
  endGame() {
    this.ngQuestionControlsHide.push('hidden');
    this.ngPaControlsHideClass.pop('hidden');
    this.title = 'Fin de partida!';
    this.ngCloseHideClass.push('hidden');
    this.endSubs  = this.showUserScore();
  }
  
  showUserScore() {
    var counter = 0;
    for (let i = 0; i < this.words.length; i++) {
      if (this.words[i].correct == true) {
        counter++;
      }
    }
    return "Has conseguido un total de " + counter + " aciertos.";
  }
  
  newGame() {
    this.ngControlsHideClass.push('hidden');
    this.ngQuestionControlsHide.pop('hidden');
    this.ngCloseHideClass.pop('hidden');
    this.showDefinition(this.count);
    this.countdown();
  }
  
  send() {
    this.checkAnswer(this.count);
    this.continuePlaying();
  }

  pasalapalabra(){
    console.log('Click en el boton pasapalabra  -> : ' + this.count);
    this.pasapalabra(this.count);
    this.continuePlaying();
  }

  onKeyPressed(event){
    if(event.key === 'Enter' || event.keyCode == "13"){
      this.send();
    } else if(event.key === 'Space' || event.keyCode == "32"){
      this.pasalapalabra();
    }
    return true
  } 

  loadGame(){
    location.reload();
  }

  closeGame(){
    this.endGame();
  }

}
